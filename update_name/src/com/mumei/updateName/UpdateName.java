package com.mumei.updateName;

import java.awt.Component;
import java.util.Date;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.User;

import com.orekyuu.javatter.controller.UserStreamController;
import com.orekyuu.javatter.plugin.JavatterPlugin;
import com.orekyuu.javatter.util.SaveData;
import com.orekyuu.javatter.view.IJavatterTab;

public class UpdateName extends JavatterPlugin{

	private final String TAG = "[update_name plugin]";
	private long user_id = -1;
	private String screen_name = null;
	private SaveData data;
	
	@Override
	public String getPluginName() {
		return "update_name plugin";
	}

	@Override
	public String getVersion() {
		// TODO Auto-generated method stub
		return "0.1";
	}

	private void Log(String text){
		System.out.println("["+new Date()+"]:"+TAG+text);
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		Log("Active");
		data = getSaveData();
		data.setDefaultValue("isSendOverMessage", true);
		data.setDefaultValue("overMessage", "@<from_screen_name> 名前が長すぎます！");
		data.setDefaultValue("isSendSuccessMessage", true);
		data.setDefaultValue("successMessage", "@<from_screen_name> さんの命令で「<update_name>」に名前を変更しました。");
		data.setDefaultValue("isSendErrorMessage", true);
		data.setDefaultValue("errorMessage", "@<from_screen_name> さんの命令で「<update_name>」に名前を変更できませんでした。");
		data.setDefaultValue("active", true);
		data.setDefaultValue("mine_only", true);
		
		
		try {
			screen_name = twitter.getScreenName();
			user_id		= twitter.getId();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch(TwitterException e){
			e.printStackTrace();
		}
		addUserStreamListener(controller);

	}

	@Override
	protected IJavatterTab getPluginConfigViewObserver() {
		return  new IJavatterTab(){
			@Override
			public Component getComponent() {
				return new SettingWindow(data);
			}
		};
	}
	
	private UserStreamController controller = new UserStreamController(){

		/* (non-Javadoc)
		 * @see com.orekyuu.javatter.controller.UserStreamController#onStatus(twitter4j.Status)
		 */
		@Override
		public void onStatus(Status status) {
			if(isUpdate(status)){
				updateExec(status);
			}
		}
	};
	
	private boolean isUpdate(Status status){
		String text =status.getText();
		if(!status.isRetweet()){
			if(text.matches("^@"+screen_name+" update_name .*")){
				boolean isMe = status.getUser().getId() == user_id;
				if(!data.getBoolean("mine_only")||isMe){
					return true;
				}
			}
		}
		return false;
	}
	

	private void updateExec(final Status status){
		new Thread(){
			@Override
			public void run(){
				try {
					String name = status.getText().replace("@"+screen_name+" update_name ", "");
					User to = status.getUser();
					if(name.length()>20){
						if(data.getBoolean("isSendOverMessage")){
							String message = replace(name,to,data.getString("overMessage"));
							StatusUpdate update = new StatusUpdate(message);
							update.setInReplyToStatusId(status.getId());
							twitter.updateStatus(update);
						}
						return;
					}
					
					User user = twitter.updateProfile(name, null, null, null);
					if(user!=null){
						Log("update: new name="+name+" user="+user.toString());
						if(data.getBoolean("isSendSuccessMessage")){
							String message = replace(name,to,data.getString("successMessage"));
							StatusUpdate update = new StatusUpdate(message);
							update.setInReplyToStatusId(status.getId());
							twitter.updateStatus(update);
						}
					}else{
						if(data.getBoolean("isSendErrorMessage")){
							String message = replace(name,to,data.getString("errorMessage"));
							StatusUpdate update = new StatusUpdate(message);
							update.setInReplyToStatusId(status.getId());
							twitter.updateStatus(update);
						}
					}
				} catch (TwitterException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	
	
	private String replace(String newName,User fromUser,String message){
		message = message.replace("<update_name>", newName);
		message = message.replace("<from_screen_name>",fromUser.getScreenName());
		message = message.replace("<from_name>",fromUser.getName());
		
		return message;
	}

}
