package com.mumei.updateName;

import javax.swing.JPanel;

import com.orekyuu.javatter.util.SaveData;
import javax.swing.JCheckBox;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JButton;

public class SettingWindow extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1112186532337345821L;
	
	
	private JCheckBox chckbxActive,chckbxMineOnly,success,over,error;
	
	private JTextArea successMessage,overMessage,errorMessage;
	
	private SaveData data;
	
	
	public SettingWindow(SaveData data){
		
		this.data = data;
		
		setLayout(null);
		setPreferredSize(new Dimension(360, 360));
		
		chckbxActive = new JCheckBox("\u6709\u52B9");
		chckbxActive.setSelected(data.getBoolean("active"));
		chckbxActive.setBounds(8, 6, 87, 21);
		add(chckbxActive);
		
		chckbxMineOnly = new JCheckBox("\u81EA\u5206\u304B\u3089\u306E\u547D\u4EE4\u306E\u307F\u6709\u52B9");
		chckbxMineOnly.setBounds(114, 6, 238, 21);
		chckbxMineOnly.setSelected(data.getBoolean("mine_only"));
		add(chckbxMineOnly);
		
		JLabel label = new JLabel("\u66F4\u65B0\u6210\u529F");
		label.setBounds(8, 33, 108, 13);
		add(label);
		
		success = new JCheckBox("\u6709\u52B9");
		success.setSelected(data.getBoolean("isSendSuccessMessage"));
		success.setBounds(114, 29, 103, 21);
		add(success);
		
		JLabel label_1 = new JLabel("\u6587\u5B57\u6570\u30AA\u30FC\u30D0\u30FC");
		label_1.setBounds(8, 106, 108, 13);
		add(label_1);
		
		over = new JCheckBox("\u6709\u52B9");
		over.setSelected(data.getBoolean("isSendOverMessage"));
		over.setBounds(114, 102, 103, 21);
		add(over);
		
		JLabel label_2 = new JLabel("\u8B0E\u306E\u30A8\u30E9\u30FC");
		label_2.setBounds(8, 179, 108, 13);
		add(label_2);
		
		error = new JCheckBox("\u6709\u52B9");
		error.setSelected(data.getBoolean("isSendErrorMessage"));
		error.setBounds(114, 175, 103, 21);
		add(error);
		
		JTextArea txtrid = new JTextArea();
		txtrid.setEditable(false);
		txtrid.setText("<update_name>\t\u66F4\u65B0\u3055\u308C\u308B\u540D\u524D\r\n<from_screen_name>\t\u547D\u4EE4\u3057\u305F\u30E6\u30FC\u30B6\u306EID\r\n<from_name>\t\t\u547D\u4EE4\u3057\u305F\u30E6\u30FC\u30B6\u306E\u540D\u524D");
		txtrid.setBounds(8, 259, 340, 64);
		add(txtrid);
		
		overMessage = new JTextArea();
		overMessage.setText(data.getString("overMessage"));
		JScrollPane scrollPane_1 = new JScrollPane(overMessage);
		scrollPane_1.setBounds(8, 129, 340, 40);
		add(scrollPane_1);
		
		errorMessage = new JTextArea();
		errorMessage.setText(data.getString("errorMessage"));
		JScrollPane scrollPane_2 = new JScrollPane(errorMessage);
		scrollPane_2.setBounds(8, 202, 340, 40);
		add(scrollPane_2);
		
		
		successMessage = new JTextArea();
		successMessage.setText(data.getString("successMessage"));
		JScrollPane scrollPane = new JScrollPane(successMessage);
		scrollPane.setBounds(8, 56, 340, 40);
		add(scrollPane);
		
		JButton button = new JButton("\u4FDD\u5B58");
		button.setBounds(8, 329, 91, 21);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
			
		});
		add(button);
		
		
		
	}
	
	private void save(){
		data.setBoolean("active", chckbxActive.isSelected());
		data.setBoolean("mine_only", chckbxMineOnly.isSelected());
		data.setBoolean("isSendSuccessMessage",success.isSelected());
		data.setBoolean("isSendOverMessage",over.isSelected());
		data.setBoolean("isSendErrorMessage",error.isSelected());
		data.setString("successMessage", successMessage.getText());
		data.setString("overMessage", overMessage.getText());
		data.setString("errorMessage", errorMessage.getText());
	}
}
